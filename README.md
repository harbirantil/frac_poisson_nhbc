# README #

This work is supported by 

      National Science Foundation (NSF) DMS - 1521590     

### What is this repository for? ###

This is a standalone Matlab code to solve Fractional Poission Equation in 2D with nonhomogeneous Dirichlet
boundary conditions. For implementation details see 

      http://math.gmu.edu/~hantil/Tech_Report/HAntil_JPfefferer_2017a.pdf
	  
Extension to 3D is straightforward.  Extensions to Neumann or other boundary conditions is straightforward 
as well by following https://arxiv.org/pdf/1703.05256.pdf	  

### Acknowledgements ###

We are thankful to Prof. Long Chen for allowing us to use gradbasis.m. We are also thankful to Prof. Andrea 
Bonito for discussions. 

### Citation ###

a. Please see the technical report for the code

@TECHREPORT{HAntil_JPfefferer_2017a,
   author      = {H. Antil and J. Pfefferer},   
   title       = {A short Matlab implementation of fractional Poission equation with nonzero boundary conditions},   
   institution = {Department of Mathematical Sciences},   
   address     = {George Mason University},   
   year        = 2017,   
   note        = "\url{http://math.gmu.edu/~hantil/Tech_Report/HAntil_JPfefferer_2017a.pdf}"   
}


b. Please see the paper H. Antil, J. Pfefferer and S. Rogovs 2017 for fractional PDEs
   with non-zero boundary conditions

@ ARTICLE{HAntil_JPfefferer_SRogovs_2017a,
  title={Fractional Operators with Inhomogeneous Boundary Conditions: Analysis, Control, and Discretization},  
  fauthor={Antil, Harbir and Pfefferer, Johannes and Rogovs, Sergejs},  
  author={Antil, H. and Pfefferer, J. and Rogovs, S.},  
  journal={arXiv preprint arXiv:1703.05256},  
  year={2017}  
}


c. Please see the papers by A. Bonito and J.E. Pasciak 2015, 2017 for solving fractional PDEs
   with zero boundary conditions

@ ARTICLE{ABonito_JEPasciak_2015a,
   FAUTHOR = {Bonito, Andrea and Pasciak, Joseph E.},   
    AUTHOR = {Bonito, A. and Pasciak, J.E.},    
     TITLE = {Numerical approximation of fractional powers of elliptic     
              operators},
   JOURNAL = {Math. Comp.},   
  FJOURNAL = {Mathematics of Computation},  
    VOLUME = {84},    
      YEAR = {2015},      
    NUMBER = {295},    
     PAGES = {2083--2110},     
      ISSN = {0025-5718},      
   MRCLASS = {65N30 (65R20)},   
  MRNUMBER = {3356020},  
MRREVIEWER = {Igor Bock},
       DOI = {10.1090/S0025-5718-2015-02937-8},       
       URL = {http://dx.doi.org/10.1090/S0025-5718-2015-02937-8},       
}

@ ARTICLE{ABonito_JEPasciak_2017a,
   FAUTHOR = {Bonito, Andrea and Pasciak, Joseph E.},   
    AUTHOR = {Bonito, A. and Pasciak, J.E.},        
     TITLE = {Numerical approximation of fractional powers of regularly     
              accretive operators},              
   JOURNAL = {IMA J. Numer. Anal.},   
  FJOURNAL = {IMA Journal of Numerical Analysis},  
    VOLUME = {37},    
      YEAR = {2017},      
    NUMBER = {3},    
     PAGES = {1245--1273},     
      ISSN = {0272-4979},      
   MRCLASS = {65J10 (26A33 65R10)},   
  MRNUMBER = {3671494},  
MRREVIEWER = {C. Ilioi},
       URL = {https://doi.org/10.1093/imanum/drw042},       
}

